<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <h1>Buat Account Baru!</h1>
</head>
<body>
    <h2>Sign Up Form</h2>

    <form action="./welcome.html">
        <label for="fname">First name: </label><br>
        <input type="text" id="fname" name="fname"><br><br>

        <label for="lname">Last name: </label><br>
        <input type="text" id="lname" name="lname"><br><br>

        <label for="gender">Gender: </label><br>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label> <br><br>

        <label for="nationality">Nationality:</label><br>
        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesian</option>
            <option value="malaysian">Malaysian</option>
            <option value="singaporean">Singaporean</option>
            <option value="australian">Australian</option>
        </select>
        <br><br>

        <label for="language">Language Spoken:</label><br>
        <input type="checkbox" id="bahasa" name="bahasa" value="bahasa">
        <label for="bahasa"> Bahasa Indonesia</label><br>
        <input type="checkbox" id="english" name="english" value="english">
        <label for="english"> English</label><br>
        <input type="checkbox" id="other" name="other" value="other">
        <label for="other"> Other</label><br><br>

        <label for="bio">Bio:</label><br>
        <textarea id="bio" name="bio" rows="4" cols="50">
        </textarea><br><br>

        <input type="submit" value="Submit">
    </form>
</body>
</html>